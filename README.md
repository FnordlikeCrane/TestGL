# TestGL
## My attempt at learning how to use OpenGL in a cross platform friendly way.
---

In this project, I'll be learning from multiple tutorial websites, mostly [lazyfoo.net](http://lazyfoo.net) and [opengl-tutorial.org](http://www.opengl-tutorial.org), in order to gain experience in OpenGL in a way that will allow me to one day make cross platform projects (particularly for Posix systems and Windows).

I hate using most popular IDEs like Visual Studio, QT Creator, etc. (Code Blocks is alright), and prefer a bare bones approach, so I'll be using MinGW and GCC for Windows and Linux respectively. I'll detail the required libraries for this project and how to get them for your OS below.

### Windows:  
I compiled all necessary libraries using cmake from their github/sourcefourge repositories.
The links for everything I used are as follows:

> *Tools*:
> * [CMake](https://cmake.org/download/)
> * [MinGW](https://sourceforge.net/projects/mingw/files/)
> * [Git for Windows](https://gitforwindows.org/) For git bash (simplicity of using git repositories + better command line environment for compiling)
> * [Atom](https://atom.io/) (For good git integrated text editing, although a bit bulky for general editing)
> * [Notepad++](https://notepad-plus-plus.org/) (Extremely lightweight good for quick editing various files that I don't feel like opening Atom again for)


> *Libraries*:
> * [FreeGLUT](http://freeglut.sourceforge.net/index.php#download) (Pretty much only for LazyFoo lessons, which don't cover 3D OpenGL)
> * [GLEW](https://github.com/nigels-com/glew)
> * [GLFW](https://github.com/glfw/glfw)
> * [GLM](https://github.com/g-truc/glm)

After compiling the libraries using CMake (*Be sure your generator is set to `MinGW Makefiles`*), I edited the `cmake_install.cmake` files to change the install directory to be located at `C:/MinGW_dev_lib` (which you'll have to create if you do it my way). Of course it doesn't matter where you install your compiled files, as long as you change the search paths in your own makefiles.  
In my Windows makefiles though, you may notice that the library search directory is actually `C:/MinGW_dev_lib/lib-x86` (not the default path afte installing the libraries). You can either change that to wherever yours are located, or move/rename your `C:/MinGW_dev_lib/lib` directory to be `C:/MinGW_dev_lib/lib-x86` as well (*I have this setup because on my PC I have separate library folders for 64 and 32 bit libraries*).  

Compilation is meant to be done through git bash (command line), and should be as simple as running `mingw32-make` (As long as the makefile is configured for your system, see the note at the bottom).  

Once everything is compiled, don't forget to move the `.dll` files from the library install directory over to wherever you are building the project's binaries, or just include them in `$PATH`/`%PATH%`.  

### GNU/Linux

To get things running on a Linux machine, you'll need to install the following packages (which could have different names if you're not on Ubuntu):

```
cmake make g++ libx11-dev libxi-dev libgl1-mesa-dev libglu1-mesa-dev libxrandr-dev libxext-dev libxcursor-dev libxinerama-dev libxi-dev freeglut3-dev libglfw3-dev
```

And then once again to build and run the program, it should be as simple as running `make all` (As long as the makefile is configured for your system, see the note at the bottom).

---
__*NOTE*__:  
You may want to delete the regular makefile, and rename `Makefile-windowsExample` or `Makefile-ubuntuExample` (Depending on whatever OS you may be using) to `Makefile` then build. Depending on what partition I've been developing this project on, the current makefile could be intended for Ubuntu development, rather than Windows MinGW, or vice versa, thus the makefile as is might not be friendly to your OS. Whatever the case, take a look at both makefiles and get an idea of what yours will need to look like.
