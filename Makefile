# MAKEFILE FOR UBUNTU
CC = g++
# The following two lines specify where your include directory and binary file directory are located. If yours aren't on these paths, you will need to change them.
SEARCH = -I/usr/include/  # This part is most likely unnecessary, as this directory is probably already on your path (on a Linux machine at least).
LIBS = -L/usr/lib/ -L/usr/lib64  # Also most likely unnecessary for the same reason.
CFLAGS = -w -Wall -Wl,-rpath,/usr/lib64/
# The following line specifies all the libraries that need to be linked in order to build the program. The path to these libraries need to be specified for the compiler to work, which is what the line starting with 'LIBS' is for.
LFLAGS = -lGL -lGLU -lglut -lglfw -lGLEW -lpthread -ldl -lm -lX11 -lXrandr -lXi -lXcursor -lXinerama -lXxf86vm
INPUT = common/*.cpp main.cpp  # This line specifies what files we're compiling to make our program.
OUT = bin/testGL  # This line specifies where we build our binary files. For the sake of git, we build in a separate directory, which is excluded from the git repo.


build:
	sh ./isbin.sh
	$(CC) $(INPUT) $(CFLAGS) $(SEARCH) $(LIBS) $(LFLAGS) -o $(OUT)

run:
	sh ./isbin.sh
	./$(OUT)

all:
	sh ./isbin.sh
	$(CC) $(INPUT) $(CFLAGS) $(SEARCH) $(LIBS) $(LFLAGS) -o $(OUT)
	./$(OUT)
