#!/bin/bash

if [ ! -d "./bin" ]; then
    mkdir bin
    if [ ! -d "./bin" ]; then
        exit 1
    else
        printf "New folder: 'bin' (executables will be stored here)\n"
    fi
#else # for testing only
#    printf "'bin' folder already exists.\n"
fi

exit 0
